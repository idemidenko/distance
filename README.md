Transactions and in-memory database are used to avoid concurrency issues.

The depth-first search is applied to find all non-cyclical paths between two nodes.


DB has some data for testing.
The following requests can be used to verify operations:

 - findDistances
```
curl -X GET 'http://localhost:9891/v1/distances?city1=A&city2=D'
```

 - createDistance
```
curl -X POST \
  http://localhost:9891/v1/distances \
  -H 'Content-Type: application/json' \
  -d '{
	"city1": "A",
	"city2": "O",
	"distance": 3
}'
```


Unit tests, api-documentation, etc. were not added as it's only a toy application.
