package com.example.distance.mapper;

import org.mapstruct.Mapper;

import com.example.distance.dto.CityDistanceDto;
import com.example.distance.model.CityDistance;

@Mapper(componentModel = "spring")
public interface CityDistanceMapper {
    CityDistance map(CityDistanceDto cityDistanceDto);
    CityDistanceDto map(CityDistance cityDistance);
}
