package com.example.distance.util;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;

import com.example.distance.dto.AdjacentCityDto;

public class Graph {
    private Map<String, LinkedHashSet<AdjacentCityDto>> map = new HashMap<>();

    private void addEdge(String node1, String node2, int distance) {
        LinkedHashSet<AdjacentCityDto> adjacent = map.computeIfAbsent(node1, e -> new LinkedHashSet<>());

        AdjacentCityDto cityDistance = new AdjacentCityDto(node2, distance);
        adjacent.add(cityDistance);
    }

    public void addTwoWayVertex(String node1, String node2, int distance) {
        addEdge(node1, node2, distance);
        addEdge(node2, node1, distance);
    }

    public LinkedList<AdjacentCityDto> adjacentNodes(String last) {
        LinkedHashSet<AdjacentCityDto> adjacent = map.get(last);
        if (adjacent == null) {
            return new LinkedList<>();
        }
        return new LinkedList<>(adjacent);
    }
}
