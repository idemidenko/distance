package com.example.distance.dto;

import java.util.LinkedList;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
public class FoundDistanceDto {
    private LinkedList<String> path;
    private int distance;
}
