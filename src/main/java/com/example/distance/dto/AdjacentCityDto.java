package com.example.distance.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(of = {"city"})
@AllArgsConstructor
@ToString
public class AdjacentCityDto {
    private String city;
    private int distance;
}
