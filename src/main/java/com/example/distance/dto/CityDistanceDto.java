package com.example.distance.dto;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class CityDistanceDto {
    private Integer id;
    @NotNull
    private String city1;
    @NotNull
    private String city2;
    @NotNull
    private int distance;
}
