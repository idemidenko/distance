package com.example.distance.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.distance.model.CityDistance;

public interface CityDistanceDao extends JpaRepository<CityDistance, Integer> {
}
