package com.example.distance.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.example.distance.model.CityDistance;

@Repository
public class CityDistanceCustomDao {
    @PersistenceContext
    private EntityManager entityManager;

    public CityDistance find(String city1, String city2) {
        Session session = entityManager.unwrap(Session.class);
        return (CityDistance) session.createCriteria(CityDistance.class)
                .add(
                        Restrictions.and(
                                Restrictions.or(
                                        Restrictions.eq("city1", city1),
                                        Restrictions.eq("city2", city1)),
                                Restrictions.or(
                                        Restrictions.eq("city1", city2),
                                        Restrictions.eq("city2", city2))
                        )
                ).uniqueResult();
    }

}
