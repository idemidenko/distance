package com.example.distance.operation;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.example.distance.dto.CityDistanceDto;
import com.example.distance.exception.ConflictException;
import com.example.distance.exception.UnprocessableEntityException;
import com.example.distance.mapper.CityDistanceMapper;
import com.example.distance.model.CityDistance;
import com.example.distance.repository.CityDistanceDao;
import com.example.distance.repository.impl.CityDistanceCustomDao;

@Component
public class CreateDistanceOperation {
    @Resource
    private CityDistanceDao cityDistanceDao;
    @Resource
    private CityDistanceCustomDao cityDistanceCustomDao;
    @Resource
    private CityDistanceMapper cityDistanceMapper;

    public CityDistanceDto createDistance(CityDistanceDto cityDistanceDto) {
        validate(cityDistanceDto);
        CityDistance cityDistance = cityDistanceMapper.map(cityDistanceDto);

        cityDistance = cityDistanceDao.save(cityDistance);

        return cityDistanceMapper.map(cityDistance);
    }

    private void validate(CityDistanceDto cityDistanceDto) {
        final String city1 = cityDistanceDto.getCity1();
        final String city2 = cityDistanceDto.getCity2();

        if (city1.equals(city2)) {
            throw new UnprocessableEntityException("Cities should be different");
        }

        CityDistance existingCityDistance = cityDistanceCustomDao.find(city1, city2);

        if (existingCityDistance != null) {
            throw new ConflictException("Distance already exists");
        }
    }
}
