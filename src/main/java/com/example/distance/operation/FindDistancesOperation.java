package com.example.distance.operation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.example.distance.dto.AdjacentCityDto;
import com.example.distance.dto.FoundDistanceDto;
import com.example.distance.exception.UnprocessableEntityException;
import com.example.distance.model.CityDistance;
import com.example.distance.repository.CityDistanceDao;
import com.example.distance.util.Graph;

@Component
public class FindDistancesOperation {
    @Resource
    private CityDistanceDao cityDistanceDao;

    public Set<FoundDistanceDto> findDistances(String city1, String city2) {
        final Set<FoundDistanceDto> foundDistances = new HashSet<>();
        final Graph graph = new Graph();

        List<CityDistance> cityDistances = cityDistanceDao.findAll();
        cityDistances.forEach(cityDistance ->
                graph.addTwoWayVertex(cityDistance.getCity1(), cityDistance.getCity2(), cityDistance.getDistance()));

        LinkedList<AdjacentCityDto> visited = new LinkedList<>();
        visited.add(new AdjacentCityDto(city1, 0));
        depthFirst(graph, visited, city2, foundDistances);

        if (foundDistances.isEmpty()) {
            throw new UnprocessableEntityException("No connection between cities");
        }

        return foundDistances;
    }

    private void depthFirst(Graph graph, LinkedList<AdjacentCityDto> visited, final String end, Set<FoundDistanceDto> foundDistances) {
        LinkedList<AdjacentCityDto> adjacentNodes = graph.adjacentNodes(visited.getLast().getCity());
        // examine adjacent nodes
        for (AdjacentCityDto node : adjacentNodes) {
            if (visited.contains(node)) {
                continue;
            }
            if (node.getCity().equals(end)) {
                visited.add(node);

                FoundDistanceDto foundDistance = createFoundDistance(visited);
                foundDistances.add(foundDistance);

                visited.removeLast();
                break;
            }
        }
        // move to the next node
        for (AdjacentCityDto node : adjacentNodes) {
            if (visited.contains(node) || node.getCity().equals(end)) {
                continue;
            }
            visited.addLast(node);
            depthFirst(graph, visited, end, foundDistances);
            visited.removeLast();
        }
    }

    private FoundDistanceDto createFoundDistance(LinkedList<AdjacentCityDto> visited) {
        int distance = visited.stream()
                .mapToInt(AdjacentCityDto::getDistance)
                .sum();
        LinkedList<String> path = visited.stream()
                .map(AdjacentCityDto::getCity)
                .collect(Collectors.toCollection(LinkedList::new));

        return new FoundDistanceDto(path, distance);
    }
}
