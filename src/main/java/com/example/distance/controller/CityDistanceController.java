package com.example.distance.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Set;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.distance.dto.FoundDistanceDto;
import com.example.distance.error.ApiError;
import com.example.distance.exception.ConflictException;
import com.example.distance.exception.UnprocessableEntityException;
import com.example.distance.dto.CityDistanceDto;
import com.example.distance.operation.CreateDistanceOperation;
import com.example.distance.operation.FindDistancesOperation;
import com.example.distance.repository.impl.CityDistanceCustomDao;

@RestController
@RequestMapping(value = "/v1/distances")
@Transactional
public class CityDistanceController {
    @Resource
    private CityDistanceCustomDao cityDistanceCustomDao;
    @Resource
    private CreateDistanceOperation createDistanceOperation;
    @Resource
    private FindDistancesOperation findDistancesOperation;

    @Transactional(readOnly = true)
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<FoundDistanceDto>> findDistances(@RequestParam("city1") String city1, @RequestParam("city2") String city2) {
        return ResponseEntity.ok(findDistancesOperation.findDistances(city1, city2));
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<CityDistanceDto> createDistance(@RequestBody @Valid CityDistanceDto cityDistanceDto) {
        return ResponseEntity.ok(createDistanceOperation.createDistance(cityDistanceDto));
    }

    @ExceptionHandler({ UnprocessableEntityException.class })
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ApiError handleUnprocessableEntityException(UnprocessableEntityException e) {
        return new ApiError(HttpStatus.UNPROCESSABLE_ENTITY.value(), e.getMessage(), e.getClass().getSimpleName());
    }

    @ExceptionHandler({ ConflictException.class })
    @ResponseBody
    @ResponseStatus(HttpStatus.CONFLICT)
    public ApiError handleConflictException(ConflictException e) {
        return new ApiError(HttpStatus.CONFLICT.value(), e.getMessage(), e.getClass().getSimpleName());
    }
}
