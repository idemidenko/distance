package com.example.distance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "DISTANCE", name = "CITY_DISTANCE")
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class CityDistance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CITY_DISTANCE_KEY")
    private Integer id;

    @Column(name = "CITY_1", nullable = false, length = 50)
    private String city1;

    @Column(name = "CITY_2", nullable = false, length = 50)
    private String city2;

    @Column(name = "DISTANCE", nullable = false)
    private int distance;
}
